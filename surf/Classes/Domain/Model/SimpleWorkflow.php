<?php
namespace Paints\Surf\Domain\Model;

use \TYPO3\Surf\Domain\Model\Node;
use \TYPO3\Surf\Domain\Model\Application;
use \TYPO3\Surf\Domain\Model\Deployment;
use TYPO3\Surf\Domain\Service\TaskManager;

class SimpleWorkflow extends \TYPO3\Surf\Domain\Model\SimpleWorkflow
{

	protected $logFilePath = '/var/www/logs';

	public function __construct(TaskManager $taskManager = NULL) {
		parent::__construct($taskManager);
		date_default_timezone_set('Europe/Berlin');
	}

	/**
	 * Execute a stage for a node and application
	 *
	 * @param string $stage
	 * @param \TYPO3\Surf\Domain\Model\Node $node
	 * @param \TYPO3\Surf\Domain\Model\Application $application
	 * @param \TYPO3\Surf\Domain\Model\Deployment $deployment
	 * @return void
	 */
	protected function executeStage($stage, Node $node, Application $application, Deployment $deployment)
	{
		if ($application->getOption('debugStages')) {
			$start = microtime(TRUE);
			$this->logMessage('Stage ' . $stage . ' started', 'surf-' . $deployment->getName());
		}

		parent::executeStage($stage, $node, $application, $deployment);

		if ($application->getOption('debugStages')) {
			$duration = round(microtime(TRUE) - $start, 2);
			$this->logMessage('Stage ' . $stage . ' finished in ' . $duration . ' seconds', 'surf-' . $deployment->getName());
		}
	}

	/**
	 * Execute a task and consider configured before / after "hooks"
	 *
	 * Will also execute tasks that are registered to run before or after this task.
	 *
	 * @param string $task
	 * @param \TYPO3\Surf\Domain\Model\Node $node
	 * @param \TYPO3\Surf\Domain\Model\Application $application
	 * @param \TYPO3\Surf\Domain\Model\Deployment $deployment
	 * @param string $stage
	 * @param array $callstack
	 * @return void
	 * @throws \TYPO3\Surf\Exception\TaskExecutionException
	 */
	protected function executeTask($task, Node $node, Application $application, Deployment $deployment, $stage, array &$callstack = array())
	{
		$dontLog = false;

		if ($application->getOption('debugTasks')) {
			$dontLog = isset($this->tasks['defined'][$task]['options']['dontLog']) ? $this->tasks['defined'][$task]['options']['dontLog'] : $dontLog;

			if (!$dontLog) {
				$start = microtime(TRUE);
				$this->logMessage('Task ' . $task . ' started', 'surf-' . $deployment->getName());
			}
		}

		parent::executeTask($task, $node, $application, $deployment, $stage, $callstack);

		if ($application->getOption('debugTasks')) {
			if (!$dontLog) {
				$duration = round(microtime(TRUE) - $start, 2);
				$this->logMessage('Task ' . $task . ' finished in ' . $duration . ' seconds', 'surf-' . $deployment->getName());
			}
		}
	}

	protected function logMessage($msg, $logFileName = 'surf.log') {
		$logFile = fopen($this->logFilePath . '/' . $logFileName . '.log', 'a');
		fwrite($logFile, date('D Y/m/d H:i:s') . ': ' . $msg . "\n");
		fclose($logFile);
	}
}
