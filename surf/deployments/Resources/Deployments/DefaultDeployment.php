<?php

require '../vendor/autoload.php';
require '../Classes/Domain/Model/SimpleWorkflow.php';

if ($debug) {
//	$stages = array(
//		'initialize',
//		'package',
//		'transfer',
//		'update',
//		'migrate',
//		'finalize',
//		'test',
//		'switch',
//		'cleanup',
//	);

	$startTime = microtime(true);
}

$debugStages = $debugStages ? $debugStages : false;
$debugTasks = $debugTasks ? $debugTasks : false;

// APPLICATION
$application = new \TYPO3\Surf\Application\TYPO3\CMS();
$application->setOption('projectName', $applicationProjectName);
$application->setOption('repositoryUrl', $applicationRepositoryUrl);
$application->setOption('typo3.surf:gitCheckout[branch]', 'master');
$application->setOption('keepReleases', 2);
$application->setOption('packageMethod', 'git');
$application->setOption('composerCommandPath', 'composer');
$application->setOption('context', $applicationContext);
$application->setOption('transferMethod', 'rsync');
$application->setOption('updateMethod', NULL);
$application->setOption('rsyncFlags', '--recursive --times --perms --links --delete --delete-excluded --exclude \'.git\' --exclude \'.sass-cache\'');
$application->setOption('folders', $applicationShareFolderSync);
$application->setOption('sourceHost', $applicationDBSourceHost);
$application->setOption('sourceUser', $applicationDBSourceUser);
$application->setOption('sourcePassword', $applicationDBSourcePassword);
$application->setOption('sourceDatabase', $applicationDBSourceDatabase);
$application->setOption('targetHost', $applicationDBTargetHost);
$application->setOption('targetUser', $applicationDBTargetUser);
$application->setOption('targetPassword', $applicationDBTargetPassword);
$application->setOption('targetDatabase', $applicationDBTargetDatabase);
$application->setOption('applicationRootDirectory', $applicationRootDirectory);
$application->setOption('webDirectory', $applicationRootDirectory);
$application->setOption('scriptFileName', 'bin/typo3cms');
$application->setOption('debugStages', $debugStages);
$application->setOption('debugTasks', $debugTasks);
$application->setDeploymentPath($applicationDeploymentPath);

// NODES
foreach ($hosts as $host => $hostData) {
	$node = new \TYPO3\Surf\Domain\Model\Node($host);
	$node->setHostname($hostData['hostname']);
	$application->addNode($node);
}

// WORKFLOW
if ($debug) {
	$workflow = new \Paints\Surf\Domain\Model\SimpleWorkflow();
} else {
	$workflow = new \TYPO3\Surf\Domain\Model\SimpleWorkflow();
}

/**
 * Log time
 */
if ($debug) {
	class LogTimeTask extends \TYPO3\Surf\Domain\Model\Task {
		public function execute(\TYPO3\Surf\Domain\Model\Node $node, \TYPO3\Surf\Domain\Model\Application $application, \TYPO3\Surf\Domain\Model\Deployment $deployment, array $options = array()) {
			if (isset($options['startTime'])) {
				logToFile(sprintf($options['message'], microtime(TRUE) - $options['startTime']), 'surf-'.$deployment->getName());
			} else {
				logToFile($options['message'], 'surf-'.$deployment->getName());
			}
		}
	}

	$logStartTimeOptions = array(
		'message' => '####### Deployment "' . $deployment->getName() . '" started',
		'dontLog' => true
	);
	$workflow->defineTask('paints.deployment:logStartTime', 'LogTimeTask', $logStartTimeOptions);
	$workflow->beforeStage('initialize', 'paints.deployment:logStartTime');
}

/**
 * Create shared folders
 */
if ($deploymentInitialDeployment) {
	$commands = array();
	foreach ($applicationShareFolderSync as $folderPair) {
		if (!is_array($folderPair) || count($folderPair) !== 2) {
			throw new InvalidConfigurationException('Each rsync folder definition must be an array of exactly two folders', 1405599056);
		}
		$commands[] = "mkdir -p " . $folderPair[1];
	}
	$createSharefolderOptions = array(
		'command' => $commands
	);

	$workflow->defineTask('paints.deployment:createSharefolderOptions', 'TYPO3\\Surf\\Task\\ShellTask', $createSharefolderOptions);
	$workflow->beforeStage('initialize', 'paints.deployment:createSharefolderOptions');
}

/**
 * Keep next symlink, to ensure apache could (re)start.
 * Needed for rollback commands
 */
$removeNextSymlinkOptions = array(
	'command' => '
        rm -f {deploymentPath}/releases/next',
	'rollbackCommand' => '
        ln -snf {currentPath} {deploymentPath}/releases/next'
);
$workflow->defineTask('paints.deployment:removeNextSymlink', 'TYPO3\\Surf\\Task\\ShellTask', $removeNextSymlinkOptions);
$workflow->beforeStage('initialize', 'paints.deployment:removeNextSymlink');

/**
 * Remove fileadmin and uploads from sync files
 *
 * Existing fileadmin or uploads path will cause a wrong symlink on live system.
 */
$removeFileadminUploadsOptions = array(
	'command' => '
		rm -rf {workspacePath}/' . $applicationRootDirectory . 'fileadmin;
		rm -rf {workspacePath}/' . $applicationRootDirectory . 'uploads',
);
$workflow->defineTask('paints.deployment:removeFileadminUploads', 'TYPO3\\Surf\\Task\\LocalShellTask',
	$removeFileadminUploadsOptions);
$workflow->beforeStage('transfer', 'paints.deployment:removeFileadminUploads');

/**
 * Set AdditionalConfiguration.php
 *
 * AdditionalConfiguration.php has to exist and has to contain at least "<?php if (!defined ('TYPO3_MODE')) die('Access denied.'); ?>"
 */
$setadditionalconfigurationOptions = array(
	'command' => '
		echo ' . escapeshellarg(file_get_contents($deploymentConfigPath . '/AdditionalConfiguration.php')) . ' > {releasePath}/Configuration/Production/Live/AdditionalConfiguration.php;
		echo ' . escapeshellarg(file_get_contents($deploymentConfigPath . '/AdditionalConfiguration.php')) . ' > {releasePath}/Configuration/Production/Staging/AdditionalConfiguration.php',
);
$workflow->defineTask('paints.deployment:setadditionalconfiguration', 'TYPO3\\Surf\\Task\\ShellTask',
	$setadditionalconfigurationOptions);
$workflow->afterStage('transfer', 'paints.deployment:setadditionalconfiguration');

/**
 * Set iniset.php
 *
 * iniset.php has to exist and has to contain at least "<?php  ?>"
 */
if ($deploymentInitialDeployment) {
	$setinisetOptions = array(
		'command' => '
		echo ' . escapeshellarg(file_get_contents($deploymentsPath . '/Resources/iniset.php')) . ' > {releasePath}/' . $applicationRootDirectory . 'typo3conf/iniset.php',
		'rollbackCommand' => '
		rm -f {currentPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php',
	);
} else {
	$setinisetOptions = array(
		'command' => '
		echo ' . escapeshellarg(file_get_contents($deploymentsPath . '/Resources/iniset.php')) . ' > {releasePath}/' . $applicationRootDirectory . 'typo3conf/iniset.php;
		echo ' . escapeshellarg(file_get_contents($deploymentsPath . '/Resources/iniset.php')) . ' > {currentPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php',
		'rollbackCommand' => '
		rm -f {currentPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php',
	);
}
$workflow->defineTask('paints.deployment:setiniset', 'TYPO3\\Surf\\Task\\ShellTask', $setinisetOptions);
$workflow->afterStage('transfer', 'paints.deployment:setiniset');

/**
 * Set file permissions
 */
$fixfilepermissionOptions = array(
//	'command' => '
//		sudo chown -R ' . $filePermissionsUser . ':' . $filePermissionsGroup . ' {releasePath};
//		sudo find {releasePath}/. -type f -print0 | sudo xargs -0 chmod 664;
//		sudo find {releasePath}/. -type d -print0 | sudo xargs -0 chmod 775;
//		sudo find {releasePath}/ -type f -name \*.sh -print0 | sudo xargs -0 chmod ug+x;
//		sudo find {releasePath}/ -type f -name \*.phpsh -print0 | sudo xargs -0 chmod ug+x;
//		sudo chmod ug+x {releasePath}/bin/typo3cms',
	'command' => '
		chown -R ' . $filePermissionsUser . ':' . $filePermissionsGroup . ' {releasePath};
		find {releasePath}/. -type f -print0 | xargs -0 chmod 664;
		find {releasePath}/. -type d -print0 | xargs -0 chmod 775;
		find {releasePath}/ -type f -name \*.sh -print0 | xargs -0 chmod ug+x;
		find {releasePath}/ -type f -name \*.phpsh -print0 | xargs -0 chmod ug+x;
		chmod ug+x {releasePath}/bin/typo3cms',
);
$workflow->defineTask('paints.deployment:fixfilepermission', 'TYPO3\\Surf\\Task\\ShellTask', $fixfilepermissionOptions);
$workflow->afterStage('transfer', 'paints.deployment:fixfilepermission');

/**
 * Set backend lock on next live
 */
if ($deploymentInitialDeployment) {
	$setbackendlockOptions = array(
		'command' => '
		    export TYPO3_CONTEXT=\'' . $applicationContext . '\';
			php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin setBElock;
			true',
		'rollbackCommand' => '
		    export TYPO3_CONTEXT=\'' . $applicationContext . '\';
			php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin clearBElock;
			true',
	);
} else {
	$setbackendlockOptions = array(
		'command' => '
		    export TYPO3_CONTEXT=\'' . $applicationContext . '\';
			php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin setBElock;
			php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin setBElock;
			true',
		'rollbackCommand' => '
		    export TYPO3_CONTEXT=\'' . $applicationContext . '\';
			php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin clearBElock;
			php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin clearBElock;
			true',
	);
}
$workflow->defineTask('paints.deployment:setbackendlock', 'TYPO3\\Surf\\Task\\ShellTask', $setbackendlockOptions);
$workflow->beforeStage('migrate', 'paints.deployment:setbackendlock');

/**
 * Change Domainname entry and add smoketest url domain if not exists
 */
if ($deploymentInitialDeployment) {
	$smokeTestParsedUrl = parse_url($smokeTestUrl);
	$sqlUpdates = 'echo "';
	foreach ($liveDomains as $liveDomain) {
		$sqlUpdates .= '
				UPDATE sys_domain
				SET domainName = \'' . $liveDomain['domainName'] . '\'
				WHERE pid = ' . $liveDomain['uid'] . ';
		';
	}
	if (isset($smokeTestParsedUrl['host']) && strlen($smokeTestParsedUrl['host'])>0) {
		$sqlUpdates .= '
          INSERT INTO sys_domain (pid, domainName) 
          SELECT \'' . $liveDomain['uid'] . '\', \'' . $smokeTestParsedUrl['host'] . '\' FROM DUAL 
          WHERE NOT EXISTS(
            SELECT * FROM sys_domain 
            WHERE pid = ' . $liveDomain['uid'] . ' AND domainName = \'' . $smokeTestParsedUrl['host'] . '\'
          );';
	}
	$sqlUpdates .= '"';
	$setdomainsOptions = array(
		'command' => $sqlUpdates . ' | mysql -u' . $applicationDBTargetUser . ' -p' . $applicationDBTargetPassword . ' -h' . $applicationDBTargetHost . ' -P' . $applicationDBTargetPort . ' ' . $applicationDBTargetDatabase,
	);
	$workflow->defineTask('paints.deployment:setdomains', 'TYPO3\\Surf\\Task\\ShellTask', $setdomainsOptions);
	$workflow->beforeStage('migrate', 'paints.deployment:setdomains');
}

/**
 * Do database backup
 */
if (!$deploymentInitialDeployment) {
	$dodatabasebackupOptions = array(
		'command' => '
			mysqldump -u' . $applicationDBTargetUser . ' -p' . $applicationDBTargetPassword . ' -h' . $applicationDBTargetHost . ' -P' . $applicationDBTargetPort . ' --default-character-set=utf8 --opt --skip-lock-tables --skip-add-locks --lock-tables=false --single-transaction --quick ' . $applicationDBTargetDatabase . ' > {currentPath}/deploymentbackup.sql',
		'rollbackCommand' => '
			mysql -u' . $applicationDBTargetUser . ' -p' . $applicationDBTargetPassword . ' -h' . $applicationDBTargetHost . ' -P' . $applicationDBTargetPort . ' ' . $applicationDBTargetDatabase . ' < {currentPath}/deploymentbackup.sql &&
			rm -f {currentPath}/deploymentbackup.sql;
			true',
	);
	$workflow->defineTask('paints.deployment:dodatabasebackup', 'TYPO3\\Surf\\Task\\ShellTask',
		$dodatabasebackupOptions);
	$workflow->beforeStage('migrate', 'paints.deployment:dodatabasebackup');
}

/**
 * Clear caches
 */
$clearStageCachesOptions = array(
	'command' => '
		export TYPO3_CONTEXT=\'' . $applicationContext . '\';
		sync;
		php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallactiveopcodecache;
		php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearconfigurationcache;
		php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearsystemcache;
		php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearpagecache;
		php {releasePath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallcaches',
	'rollbackCommand' => '
		export TYPO3_CONTEXT=\'' . $applicationContext . '\';
		sync;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallactiveopcodecache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearconfigurationcache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearsystemcache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearpagecache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallcaches',
);
$workflow->defineTask('paints.deployment:clearstagecaches', 'TYPO3\\Surf\\Task\\ShellTask', $clearStageCachesOptions);
$workflow->beforeStage('test', 'paints.deployment:clearstagecaches');
$workflow->afterStage('test', 'paints.deployment:clearstagecaches');

$clearLiveCachesOptions = array(
	'command' => '
		export TYPO3_CONTEXT=\'' . $applicationContext . '\';
		sync;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallactiveopcodecache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearconfigurationcache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearsystemcache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearpagecache;
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh extbase cacheapi:clearallcaches',
);
$workflow->defineTask('paints.deployment:clearlivecaches', 'TYPO3\\Surf\\Task\\ShellTask', $clearLiveCachesOptions);
$workflow->afterStage('switch', 'paints.deployment:clearlivecaches');
$workflow->afterStage('cleanup', 'paints.deployment:clearlivecaches');
if (!$deploymentInitialDeployment) {
	$workflow->afterStage('test', 'paints.deployment:clearlivecaches');
}

/**
 * Update Languages
 */
$updateLanguagesOptions = array(
	'command' => '
		export TYPO3_CONTEXT=\'' . $applicationContext . '\';
		sync;
		php {releasePath}/bin/typo3cms language:update',
);
$workflow->defineTask('paints.deployment:updateLanguages', 'TYPO3\\Surf\\Task\\ShellTask', $updateLanguagesOptions);
$workflow->afterStage('finalize', 'paints.deployment:updateLanguages');

/**
 * Clear Opcode caches
 */
$clearOpcodeCacheCreateFileOptions = array(
	'command' => '
			mkdir -p ' . escapeshellarg($opcodeScriptBasePath) .';
			chown -R ' . $filePermissionsUser . ':' . $filePermissionsGroup . ' ' . escapeshellarg($opcodeScriptBasePath) .';
			cd ' . escapeshellarg($opcodeScriptBasePath) .';
			rm -f surf-opcache-reset-' . $opcodeScriptIdentifier . '.php;
			echo ' . escapeshellarg(file_get_contents($deploymentsPath . '/Resources/surf-opcache-reset-default.php')) . ' > ' . escapeshellarg($opcodeScriptBasePath) . '/surf-opcache-reset-' . $opcodeScriptIdentifier . '.php;
			true',
	'rollbackCommand' => '
			cd ' . escapeshellarg($opcodeScriptBasePath) . ';
			rm -f surf-opcache-reset-' . $opcodeScriptIdentifier . '.php;
			true',
);
$workflow->defineTask('paints.deployment:clearOpcodecacheCreateFile',
	'TYPO3\\Surf\\Task\\ShellTask',
	$clearOpcodeCacheCreateFileOptions);
$workflow->afterStage('transfer', 'paints.deployment:clearOpcodecacheCreateFile');

$opcodeCacheCounter = 0;
foreach ($opcodeBaseUrls as $opcodeBaseUrl) {
	$opcodeCacheCounter++;
	$clearOpcodeCacheOptions = array(
		'scriptIdentifier' => $opcodeScriptIdentifier,
		'baseUrl' => $opcodeBaseUrl,
	);
	$workflow->defineTask('paints.deployment:clearOpcodecache'. $opcodeCacheCounter,
		'TYPO3\\Surf\\Task\\Php\\WebOpcacheResetExecuteTask',
		$clearOpcodeCacheOptions);
	$workflow->afterStage('finalize', 'paints.deployment:clearOpcodecache'. $opcodeCacheCounter);
	$workflow->afterStage('switch', 'paints.deployment:clearOpcodecache' . $opcodeCacheCounter);
	$workflow->afterStage('test', 'paints.deployment:clearOpcodecache' . $opcodeCacheCounter);
	$workflow->afterStage('cleanup', 'paints.deployment:clearOpcodecache' . $opcodeCacheCounter);
}

/**
 * Activate smoke test domain record
 */
$setSmokeTestOptions = array(
	'command' => setSmokeTestDomainRecord($smokeTestUrl, 0) . ' | mysql -u' . $applicationDBTargetUser . ' -p' . $applicationDBTargetPassword . ' -h' . $applicationDBTargetHost . ' -P' . $applicationDBTargetPort . ' ' . $applicationDBTargetDatabase,
);
$workflow->defineTask('paints.deployment:activateSmokeTestDomainRecord', 'TYPO3\\Surf\\Task\\ShellTask', $setSmokeTestOptions);
$workflow->beforeTask('paints.deployment:smoketest', 'paints.deployment:activateSmokeTestDomainRecord');

/**
 * Smoke test
 */
$smokeTestOptions = array(
	'url' => $smokeTestUrl,
	'remote' => FALSE,
	'expectedRegexp' => $smokeTestRegExp,
	'expectedStatus' => 200
);
$workflow->defineTask('paints.deployment:smoketest', 'TYPO3\\Surf\\Task\\Test\\HttpTestTask', $smokeTestOptions);
$workflow->addTask('paints.deployment:smoketest', 'test');

/**
 * Deactivate smoke test domain record
 */
$setSmokeTestOptions = array(
	'command' => setSmokeTestDomainRecord($smokeTestUrl, 1) . ' | mysql -u' . $applicationDBTargetUser . ' -p' . $applicationDBTargetPassword . ' -h' . $applicationDBTargetHost . ' -P' . $applicationDBTargetPort . ' ' . $applicationDBTargetDatabase,
);
$workflow->defineTask('paints.deployment:deactivateSmokeTestDomainRecord', 'TYPO3\\Surf\\Task\\ShellTask', $setSmokeTestOptions);
$workflow->afterTask('paints.deployment:smoketest', 'paints.deployment:deactivateSmokeTestDomainRecord');

/**
 * Keep next symlink, to ensure apache could (re)start
 */
$keepNextSymlinkOptions = array(
	'command' => '
            ln -snf {currentPath} {deploymentPath}/releases/next'
);
$workflow->defineTask('paints.deployment:keepNextSymlink', 'TYPO3\\Surf\\Task\\ShellTask', $keepNextSymlinkOptions);
$workflow->afterStage('switch', 'paints.deployment:keepNextSymlink');

/**
 * Remove backend locks
 */
$removebackendlockOptions = array(
	'command' => '
	    export TYPO3_CONTEXT=\'' . $applicationContext . '\';
		php {currentPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin clearBElock;
		php {previousPath}/' . $applicationRootDirectory . 'typo3/cli_dispatch.phpsh lowlevel_admin clearBElock;
		true;',
);
$workflow->defineTask('paints.deployment:removebackendlock', 'TYPO3\\Surf\\Task\\ShellTask', $removebackendlockOptions);
$workflow->beforeStage('cleanup', 'paints.deployment:removebackendlock');

/**
 * Remove iniset.php
 */
if ($deploymentInitialDeployment) {
	$removeinisetOptions = array(
		'command' => '
            rm -f {currentPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php;
            true',
	);
} else {
	$removeinisetOptions = array(
		'command' => '
            rm -f {currentPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php;
            rm -f {previousPath}/' . $applicationRootDirectory . 'typo3conf/iniset.php;
            true',
	);
}
$workflow->defineTask('paints.deployment:removeiniset', 'TYPO3\\Surf\\Task\\ShellTask', $removeinisetOptions);
$workflow->addTask('paints.deployment:removeiniset', 'cleanup');

/**
 * Compress database backup
 */
if (!$deploymentInitialDeployment) {
	$compressdatabasebackupOptions = array(
		'command' => '
			gzip {previousPath}/deploymentbackup.sql;
			true',
	);
	$workflow->defineTask('paints.deployment:compressdatabasebackup', 'TYPO3\\Surf\\Task\\ShellTask',
		$compressdatabasebackupOptions);
	$workflow->addTask('paints.deployment:compressdatabasebackup', 'cleanup');
}

/**
 * Remove opcode clearcache file
 */
$removeOpcodeCacheFileOptions = array(
	'command' => '
			cd ' . escapeshellarg($opcodeScriptBasePath) . ';
			rm -f surf-opcache-reset-' . $opcodeScriptIdentifier . '.php;
			true',
);
$workflow->defineTask('paints.deployment:removeOpcodeCacheFile',
	'TYPO3\\Surf\\Task\\ShellTask',
	$removeOpcodeCacheFileOptions);
$workflow->afterStage('cleanup', 'paints.deployment:removeOpcodeCacheFile');

/**
 * Log end time
 */
if ($debug) {
	$logEndTimeOptions = array(
		'startTime' => $startTime,
		'message' => 'Deployment "' . $deployment->getName() . '" finished in %f seconds #######',
		'dontLog' => true
	);
	$workflow->defineTask('paints.deployment:logEndTime', 'LogTimeTask', $logEndTimeOptions);
	$workflow->afterStage('cleanup', 'paints.deployment:logEndTime');
}

/**
 * @param $smokeTestUrl
 * @param int $hidden
 * @return string SQL statement
 */
function setSmokeTestDomainRecord($smokeTestUrl, $hidden = 1) {
	$smokeTestParsedUrl = parse_url($smokeTestUrl);
	$sqlUpdates = 'echo "';

	if (isset($smokeTestParsedUrl['host']) && strlen($smokeTestParsedUrl['host']) > 0) {
		$sqlUpdates .= '
			UPDATE sys_domain 
			SET hidden='.$hidden.'
			WHERE domainName = \''.$smokeTestParsedUrl['host'].'\'';
	}
	$sqlUpdates .= '"';

	return $sqlUpdates;
}

/**
 * Log deployment times
 */
if ($debug) {
//	function logTime($msg, $logFilePath) {
//		return array(
//			'command' => '
//			echo $(date \'+%a %Y/%m/%d %H:%M:%S\'): '.$msg.' >> ' . $logFilePath,
//			'rollbackCommand' => '
//			echo $(date \'+%a %Y/%m/%d %H:%M:%S\'): ROLLBACK: '.$msg.' >> ' . $logFilePath,
//		);
//	}

	function logToFile($msg, $logFileName = 'surf') {
		$logFilePath = '/var/www/logs/'.$logFileName.'.log';
		$logFile = fopen($logFilePath, 'a');
		date_default_timezone_set('Europe/Berlin');
		fwrite($logFile, date('D Y/m/d H:i:s') . ': ' . $msg . "\n");
		fclose($logFile);
	}
}

// DEPLOYMENT
$deployment->setOption('initialDeployment', $deploymentInitialDeployment);
$deployment->setWorkflow($workflow);

$deployment->onInitialize(function() use ($workflow, $application) {
	$workflow->removeTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\CreatePackageStatesTask');
	$workflow->removeTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\CompareDatabaseTask');
	$workflow->removeTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\FlushCachesTask');
	$workflow->removeTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\SetUpExtensionsTask');
});

$deployment->addApplication($application);