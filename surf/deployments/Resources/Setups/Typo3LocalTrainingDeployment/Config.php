<?php
$deploymentInitialDeployment = FALSE;

$applicationProjectName = 'TYPO3 BA Surf Deployment';
$applicationRepositoryUrl = 'git@gitlab.paints.de:b.strilziw/typo3_ba.git';
$applicationDeploymentPath = '/var/www/sites/paints/ba';
$applicationContext = 'Development/Training';
$applicationRootDirectory = 'Web/';

$applicationShareFolderSync = array(
	array('/var/www/typo3/Web/fileadmin', '{sharedPath}/Data/fileadmin'),
	array('/var/www/typo3/Web/uploads', '{sharedPath}/Data/uploads')
);

$filePermissionsUser = 'paints';
$filePermissionsGroup = 'www-data';

$smokeTestUrl = 'http://ba-typo3-next.paints-training.de/';
$smokeTestRegExp = '/Introduction/';

// Node Settings
$hosts = array(
	'ba-typo3.paints-training.de' => array(
		'hostname' => 'paints-training',
	),
);

// Live Domains
$liveDomains = array(
	array(
		'uid' => 1,
		'domainName' => 'typo3.live',
	),
);

// Opcode Clearcache
$opcodeScriptIdentifier = 'typo3Ba';
$opcodeScriptBasePath = '/var/www/service/opcode';
$opcodeBaseUrls = array(
	'http://service.paints-training.de/',
);

$deploymentInitialDeployment = TRUE;
$debug = TRUE;
$debugStages = TRUE;
$debugTasks = TRUE;