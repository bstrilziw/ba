<?php
$deploymentInitialDeployment = FALSE;

$applicationProjectName = 'TYPO3 BA Surf Deployment';
$applicationRepositoryUrl = 'git@gitlab.paints.de:b.strilziw/typo3_ba.git';
$applicationDeploymentPath = '/var/www/typo3-live';
$applicationContext = 'Development/Vagrant';
$applicationRootDirectory = 'Web/';

$applicationShareFolderSync = array(
	array('/var/www/typo3/Web/fileadmin', '{sharedPath}/Data/fileadmin'),
	array('/var/www/typo3/Web/uploads', '{sharedPath}/Data/uploads')
);

$filePermissionsUser = 'vagrant';
$filePermissionsGroup = 'www-data';

$smokeTestUrl = 'http://next.typo3.live/';
$smokeTestRegExp = '/Introduction/';

// Node Settings
$hosts = array(
	'typo3.live' => array(
		'hostname' => 'typo3-live',
	),
);

// Live Domains
$liveDomains = array(
	array(
		'uid' => 1,
		'domainName' => 'typo3.live',
	),
);

// Opcode Clearcache
$opcodeScriptIdentifier = 'typo3Ba';
$opcodeScriptBasePath = '/var/www/service/opcode';
$opcodeBaseUrls = array(
	'http://service.typo3.live/',
);

$deploymentInitialDeployment = TRUE;
$debug = TRUE;
$debugStages = TRUE;
$debugTasks = TRUE;