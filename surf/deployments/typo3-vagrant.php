<?php

/** @var \TYPO3\Surf\Domain\Model\Deployment $deployment */
$deploymentsPath = dirname(__FILE__);

$deploymentConfigPathAndFilename = $deploymentsPath . '/Resources/Setups/Typo3LocalVagrantDeployment/Config.php';
if (!file_exists($deploymentConfigPathAndFilename)) {
	exit(sprintf("The deployment config file %s does not exist.\n", $deploymentConfigPathAndFilename));
}
require_once($deploymentConfigPathAndFilename);

$deploymentSecureConfigPathAndFilename = $deploymentsPath . '/Resources/Secrets/Typo3LocalVagrantDeployment/Config.php';
if (!file_exists($deploymentSecureConfigPathAndFilename)) {
	exit(sprintf("The deployment config file %s does not exist.\n", $deploymentSecureConfigPathAndFilename));
}
require_once($deploymentSecureConfigPathAndFilename);

$deploymentTypePathAndFilename = $deploymentsPath . '/Resources/Deployments/DefaultDeployment.php';
if (!file_exists($deploymentTypePathAndFilename)) {
	exit(sprintf("The deployment type file %s does not exist.\n", $deploymentTypePathAndFilename));
}
require_once($deploymentTypePathAndFilename);
