# Remove default classes
RTE.classes >
RTE.classes {
	two-columns {
		name = Zweispaltige Darstellung
	}
	text-left {
		name = Linksbündig
	}
	text-center {
		name = Zentriert
	}
	text-right {
		name = Rechtsbündig
	}
	big-bullets {
		name = Big Bullets
	}
}

# Remove default anchor classes
RTE.classesAnchor >
RTE.classesAnchor {
	page_external {
		titleText =
		class = external
		type = page
		target = _blank
	}

	url_external {
		titleText =
		class = external
		type = url
		target = _blank
	}
}

RTE {
	config.tt_content >

	default {
		contentCSS = Resources/StyleSheets/rte.css
		
		showButtons (
			blockstylelabel, blockstyle, textstylelabel, textstyle,
			formatblock, bold, italic, underline, citation, orderedlist, unorderedlist, insertcharacter, link, findreplace, chMode, removeformat, undo, redo, about, image
		)

		classesAnchor = page_external, url_external

		buttons {
			blockstyle.tags {
				p.allowedClasses = two-columns, three-columns, text-left, text-right, text-center
				div.allowedClasses = two-columns, three-columns, text-left, text-right, text-center
				table.allowedClasses =
				td.allowedClasses =
				blockquote.allowedClasses = smallquote
				ul.allowedClasses = emphasized
				ol.allowedClasses = big-bullets, two-columns
			}
			blockstyle.showTagFreeClasses = 0
			link.properties.class.allowedClasses >
			link.properties.class.allowedClasses = external
			textstyle.tags.span.allowedClasses >
			textstyle.tags.span.allowedClasses =
			textstyle.showTagFreeClasses = 0
			formatblock.removeItems = pre,address,article,footer,header,aside,section,div
			formatblock.orderItems = p, h1, h2, h3, h4, h5, h6, blockquote, nav
		}

		proc {
			allowTagsOutside := addToList(cite)
			allowedClasses = two-columns, three-columns, text-left, text-right, text-center, big-bullets
			entryHTMLparser_db {
				tags {
					span {
						fixAttrib.style.unset = 1
						allowedAttribs = id, title, class
					}
				}
			}
		}
	}
	

}

RTE.default.FE >
RTE.default.FE < RTE.default