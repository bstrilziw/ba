<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Skin Paints',
    'description' => 'Paints Skin extension',
    'category' => 'templates',
    'author' => 'Benjamin Strilziw',
    'author_email' => 'b.strilziw@paints.de',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '1.0.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);