#!/usr/bin/env bash

IP=$(getDropletIpByDropletName $DROPLET_NAME)
scp /home/benjamin/.ssh/gitlab $SSH_USER@$IP:~/.ssh/bitbucket

GIT_REPOS="bitbucket.org 104.192.143.1"
echo "Adding SSH keys"
ssh $SSH_USER@$IP "ssh-keyscan -t rsa $GIT_REPOS >> ~/.ssh/known_hosts"

echo "Setting up .ssh/config"
ssh $SSH_USER@$IP 'echo "
Host bitbucket.org
 HostName bitbucket.org
 IdentityFile ~/.ssh/bitbucket

Host gitlab.paints.de
 HostName gitlab.paints.de
 User git
 IdentityFile ~/.ssh/bitbucket
" >> ~/.ssh/config'
