#!/usr/bin/env bash

echo "Copy files"
# -a archive mode
# -v verbose
# -z compress
rsync -avz \
	$LOCAL_PROJECT_PATH/fileadmin \
	$LOCAL_PROJECT_PATH/uploads \
	$SSH_USER@$IP:$DEPLOYMENT_PATH/application/Web