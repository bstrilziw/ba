#!/usr/bin/env bash

source ./configs/ba_typo3.sh

# get IP from the droplet and ssh into it
# doctl compute ssh user@droplet_name isn't working
ssh $SSH_USER@$IP
