#!/usr/bin/env bash

echo "Installing docker project in $DEPLOYMENT_PATH"
ssh $SSH_USER@$IP "git clone $DOCKER_GIT $DEPLOYMENT_PATH"
