#!/usr/bin/env bash

# execute remote install of docker-compose on the coreos machine
IP=$(getDropletIpByDropletName $DROPLET_NAME)
ssh $SSH_USER@$IP 'sudo mkdir -p /opt/bin && \
	sudo curl -L https://github.com/docker/compose/releases/download/1.10.0/docker-compose-`uname -s`-`uname -m` > docker-compose && \
	sudo mv docker-compose /opt/bin/docker-compose && \
	sudo chmod +x /opt/bin/docker-compose'
