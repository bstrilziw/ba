#!/usr/bin/env bash

if ! checkIfDropletExistsByDropletName $DROPLET_NAME;
then
	# remove known_hosts entry from file
	DROPLET_IP=$(getDropletIpByDropletName $DROPLET_NAME)
	echo "Removing $DROPLET_IP from known_hosts"
	sed -i '/'$DROPLET_IP'/d' ~/.ssh/known_hosts
	# force delete the droplet
	echo "Deleting droplet '$DROPLET_NAME'"
	doctl compute droplet delete $DROPLET_NAME --force
	sleep 10
fi
