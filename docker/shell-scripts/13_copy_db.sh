#!/usr/bin/env bash

echo "Copy DB"
docker exec $LOCAL_DB_CONTAINER mysqldump -u$LOCAL_DB_USER -p$LOCAL_DB_PASSWORD $LOCAL_DB_NAME | ssh $SSH_USER@$IP "docker exec $DEPLOYMENTPATH_db_1 mysql -u$REMOTE_DB_USER -p$REMOTE_DB_PASSWORD $REMOTE_DB_NAME"
