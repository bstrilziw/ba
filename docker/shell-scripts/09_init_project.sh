#!/usr/bin/env bash

echo "Cloning TYPO3 project in $DEPLOYMENT_PATH/application"
ssh $SSH_USER@$IP "cd $DEPLOYMENT_PATH && git clone $PROJECT_GIT application"
