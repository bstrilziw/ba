#!/usr/bin/env bash

source tools.sh

   ./05_delete_droplet.sh \
&& ./02_create_coreos_droplet.sh \
&& ./06_install_docker-compose.sh \
&& ./07_copy_ssh_key.sh \
&& ./08_install_docker_project.sh \
&& ./09_init_project.sh \
&& ./10_composer_install.sh
