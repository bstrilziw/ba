#!/usr/bin/env bash

echo "Running composer install in $DEPLOYMENT_PATH/application"
ssh $SSH_USER@$IP "eval `ssh-agent` \
	cd $DEPLOYMENT_PATH/application \
 	&& docker run --rm -i \
		 --volume \$PWD:/app \
		 --volume \$SSH_AUTH_SOCK:/ssh-auth.sock \
		 --env SSH_AUTH_SOCK=/ssh-auth.sock \
		 --env SERVER_CONTEXT=coreos \
		 --volume /etc/passwd:/etc/passwd:ro \
		 --volume /etc/group:/etc/group:ro \
		 --volume \$HOME/.ssh/:/root/.ssh/ \
		 composer install"
