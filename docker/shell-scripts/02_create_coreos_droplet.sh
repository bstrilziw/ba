#!/usr/bin/env bash

source tools.sh

REGION="fra1"
IMAGE="coreos-stable"
SIZE="512mb"
#SSH_FINGERPRINT=$([[ $(ssh-keygen -E md5 -lf ~/.ssh/id_rsa) =~ MD5:([0-9a-f:]*)[[:space:]] ]]" && echo ${BASH_REMATCH[1]})
#SSH_FINGERPRINT=$(echo $(ssh-keygen -E md5 -lf ~/.ssh/id_rsa) | egrep '[a-f0-9:]*')
SSH_FINGERPRINT="0f:c9:77:70:90:db:59:7a:08:4d:b1:69:7b:84:d8:35"

if checkIfDropletExistsByDropletName $DROPLET_NAME;
then
	# create a droplet with the given data and wait for it to finish the creation
	echo "Creating droplet '$DROPLET_NAME'"
	doctl compute droplet create $DROPLET_NAME --region $REGION --image $IMAGE --size $SIZE --ssh-keys $SSH_FINGERPRINT --enable-private-networking --wait
	sleep 20
	# add server to known_hosts
	DROPLET_IP=$(getDropletIpByDropletName $DROPLET_NAME)
	echo "Adding $DROPLET_IP to known_hosts"
	ssh-keyscan -t rsa $DROPLET_IP >> ~/.ssh/known_hosts
else
	echo "Droplet '$DROPLET_NAME' already exists"
	exit 1
fi
