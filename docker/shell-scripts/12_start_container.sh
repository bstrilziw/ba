#!/usr/bin/env bash

echo "Starting container"
ssh $SSH_USER@$IP "cd $DEPLOYMENT_PATH && docker-compose up -d"