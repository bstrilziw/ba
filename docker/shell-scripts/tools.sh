#!/usr/bin/env bash

#DROPLET_NAME="coreos-docker"
#SSH_USER="core"

function getDropletIpByDropletName {
	# get the list of all droplets, get the one line with the droplet name, get all IP addresses, get the first one
	echo "$(doctl compute droplet list | grep $1 | grep -oE '([0-9]{1,3}[\.]){3}[0-9]{1,3}' | head -n 1)"
}

function checkIfDropletExistsByDropletName {
	# get the list of all droplets, get the one line with the droplet name, count lines, if there is a droplet, it will return 1 or higher
	if [ $(doctl compute droplet list | grep $1 | wc -l) -gt 0 ]
	then
		return 1
	else
		return 0
	fi
}

function addIpToKnownHosts {
	# add server to known_hosts
	DROPLET_IP=$(getDropletIpByDropletName $DROPLET_NAME)
	echo "Adding $DROPLET_IP to known_hosts"
	ssh-keyscan -t rsa $DROPLET_IP >> ~/.ssh/known_hosts
}