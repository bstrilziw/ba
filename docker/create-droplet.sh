#!/usr/bin/env bash

# checks if file exists, if file has .sh extension and path begins with configs
if [ -e ./$1 ] && [ ${1: -3} == ".sh" ] && [ $1 == configs/* ]; then
	echo "Reading $1"
	source $1
else
	echo "Please enter a valid config file from configs/"
	exit 1
fi

source ./shell-scripts/05_delete_droplet.sh && \
source ./shell-scripts/02_create_coreos_droplet.sh && \
source ./shell-scripts/06_install_docker-compose.sh && \
source ./shell-scripts/07_copy_ssh_key.sh && \
source ./shell-scripts/08_install_docker_project.sh && \
source ./shell-scripts/09_init_project.sh && \
source ./shell-scripts/10_composer_install.sh
