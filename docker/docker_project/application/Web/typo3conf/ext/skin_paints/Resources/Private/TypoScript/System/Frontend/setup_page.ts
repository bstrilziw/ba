config {
    # How to handle locallization
    sys_language_mode = {$config.sys_language_mode}

    # Records that are not locallized till be hidden
    sys_language_overlay = {$config.sys_language_overlay}

    locale_all = {$config.locale_all}

    htmlTag_langKey = {$config.language}

    # set doctype to html5
    doctype = html5

    # Remove default css for all extensions
    removeDefaultCss = 1

    # Enable site indexing
    index_enable = 1

    # concat JS
    concatenateJs = 1

    # set absRefPrefix, to get rid of basetag
    absRefPrefix = /
}

# Create basic page object
page = PAGE
page.typeNum = 0

page.config {
    # remove xmlprologue
    xmlprologue = none

    # Tries to clean up the output. Tries...
    xhtml_cleaning = all

    # remove comments
    disablePrefixComment = 1

    # If set, the inline styles TYPO3 controls in the core are written to a file
    # done by scriptmerger, does not work with CSS_styleInline
    inlineStyle2TempFile = 1

    # All javascript (includes and inline) will be moved to the bottom of the html document
    moveJsFromHeaderToFooter = 0

    # Add L and print values to all links in TYPO3.
    linkVars = {$page.config.linkVars}

    # this must be set to the language of the site
    language = {$page.config.language}

    # System Language ID
    sys_language_uid = {$page.config.sys_language_uid}

    # Enable RealURL
    tx_realurl_enable = 1

    # Enable full path for anchor links
    prefixLocalAnchors = all

    # Disable simulateStaticDocuments
    simulateStaticDocuments = 0

    # Charset, should always be utf-8
    renderCharset = utf-8

    # Charset, should always be utf-8
    metaCharset = utf-8

    # If set, then every typolink is checked whether it's linking to a page within the current rootline of the site.
    typolinkCheckRootline = 1

    # This option enables to create links across domains using current domain's linking scheme.
    typolinkEnableLinksAcrossDomains = 1

    # overwrites removeDefaultJS = 1, that disables the email spam protection
    removeDefaultJS = external

    headerComment = Conception, design, programming and service providing: Paints Multimedia GmbH
}

# Enable different browser switch
config.config.htmlTag_setParams = class="no-js"

# Meta Tags
page.meta {
    DESCRIPTION.field = description

    KEYWORDS.field = keywords

    viewport = width=device-width,initial-scale=1

    X-UA-Compatible = IE=edge,chrome=1
    X-UA-Compatible.httpEquivalent = 1

    robots.stdWrap.cObject = COA
    robots.stdWrap.cObject {
        10 = TEXT
        10.value = index,follow
        10.override = noindex
        10.override.if.isTrue.field = no_search
    }
}

[globalVar = TSFE:id = {$page.settings.home}]
    page.meta {
        robots.stdWrap.cObject {
            20 = TEXT
            20.value = ,noodp
        }
    }
[global]

page.10 = FLUIDTEMPLATE
page.10 {
    templateName.stdWrap.cObject = TEXT
    templateName.stdWrap.cObject {
        data = levelfield:-2,backend_layout_next_level,slide
        override.field = backend_layout
        split {
            token = file__
            1.current = 1
            1.wrap = |
        }

        ifEmpty = Default
    }

    settings {
        home = {$page.settings.home}

        logo {
            src = {$page.settings.logo.src}
            width = {$page.settings.logo.width}
            height = {$page.settings.logo.height}
        }

        menu {
            menuType = {$page.settings.menu.menuType}
            show = {$page.settings.menu.show}
            main = {$page.settings.menu.main}
            top = {$page.settings.menu.top}
            footer = {$page.settings.menu.footer}
            mainnavExcludePages = {$page.settings.menu.mainnavExcludePages}
        }

        pages {
            menuContent = {$page.settings.pages.menuContent}
            footerContent = {$page.settings.pages.footerContent}
        }

        cols {
            mainContentColumn = {$page.settings.cols.mainContentColumn}
        }

        links {
            protocol = {$page.config.protocol}
        }

        keys {
            googleTagManagerId = {$page.settings.keys.googleTagManagerId}
        }

        footer {
            link = {$page.settings.footer.link}
            topContent = {$page.settings.footer.topContent}
            bottomContent = {$page.settings.footer.bottomContent}

            menuType = {$page.settings.footer.menuType}
        }
    }

    templateRootPaths.100 = {$plugin.tx_skin_paints.view.templateRootPath}
    partialRootPaths.100 = {$plugin.tx_skin_paints.view.partialRootPath}
    layoutRootPaths.100 = {$plugin.tx_skin_paints.view.layoutRootPath}

    extbase.controllerExtensionName = SkinPaints
}

page.shortcutIcon = {$page.config.favicon}}

page.includeCSS {
	style = /Resources/StyleSheets/style.css
	print = /Resources/StyleSheets/print.css
}

# Include Stylesheet
[globalString = LIT:{$page.settings.keys.webfonts} = /(.+)/]
    page.includeCSS {
        webfonts = //fast.fonts.net/cssapi/{$page.settings.keys.webfonts}.css
        webfonts.external = 1
        webfonts.excludeFromConcatenation = 1
    }
[global]

[globalString = LIT:{$page.settings.keys.googleFonts} = /(.+)/]
    page.includeCSS {
        googleWebfont = //fonts.googleapis.com/css?family={$page.settings.keys.googleFonts}
        googleWebfont.external = 1
        googleWebfont.excludeFromConcatenation = 1
    }
[global]

# Include JavaScripts
page.includeJSlibs {
    modernizr = EXT:skin_paints/Resources/Public/JavaScripts/modernizr.js
}

# remove all included Libs and include them again, because of missing dependency feature
page.includeJSFooterlibs >
page.includeJSFooterlibs {
    main = /Resources/JavaScripts/main.js
}

lib.sectionMenu < tt_content.menu.20.3
lib.sectionMenu.special.value >
lib.sectionMenu.special.value = {$page.settings.home}
lib.sectionMenu.stdWrap.outerWrap = <nav class="mainmenu section-anchor-menu"><ul class="linklist">|</ul></nav>
