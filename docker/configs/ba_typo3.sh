#!/usr/bin/env bash

source shell-scripts/tools.sh

DROPLET_NAME="coreos-docker"
SSH_USER="core"
IP="$(getDropletIpByDropletName $DROPLET_NAME)"
PROJECT_NAME="ba_typo3"
DEPLOYMENT_PATH="~/$(date +%Y%m%d%H%M%S)_$PROJECT_NAME"
LOCAL_PROJECT_PATH="/home/benjamin/projects/ba/docker/docker_project/application/Web"
DOCKER_GIT="git@bitbucket.org:bstrilziw/docker_ba_typo3.git"
PROJECT_GIT="git@bitbucket.org:bstrilziw/typo3_ba.git"
LOCAL_DB_CONTAINER="ba_typo3_db_1"
LOCAL_DB_NAME="typo3-docker"
LOCAL_DB_USER="typo3-docker"
LOCAL_DB_PASSWORD="typo3-docker"
REMOTE_DB_NAME="typo3-docker"
REMOTE_DB_USER="typo3-docker"
REMOTE_DB_PASSWORD="typo3-docker"
